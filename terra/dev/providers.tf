terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.27"
    }
  }

  required_version = ">= 0.13.0"
}

terraform {
  backend "s3" {
    bucket = "test-bnk-gitlab"
    key    = "state"
    region = "us-east-1"
  }
}
