# EC2 INSTANCE CREATE

resource "aws_instance" "ec2-app" {
  ami= "ami-04505e74c0741db8d"
  subnet_id = data.aws_subnet.dsubnet-pub1-app.id
  instance_type = "t2.micro"
  key_name = var.key_name
  user_data = file("../modules/compute/ec2-userdata.sh")
  vpc_security_group_ids = [data.aws_security_group.dsg-pub-app.id]
  tags = {
    Name = "ec2-app"
  }
}