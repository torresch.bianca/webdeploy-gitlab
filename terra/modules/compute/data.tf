data "aws_subnet" "dsubnet-pub1-app" {
  id = var.sn_pub1
}

data "aws_security_group" "dsg-pub-app" {
  id = var.sgp_pub
}