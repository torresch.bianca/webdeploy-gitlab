output "public-ip-app" {
  value = aws_instance.ec2-app.public_ip
}

output "ec2_instance_id" {
  value = aws_instance.ec2-app.id
}